package com.example.Garam_;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

/*
수화 Activity
*/
public class HydrationActivity extends Activity implements View.OnClickListener {
ImageButton close_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hydration);

        close_btn = (ImageButton)findViewById(R.id.close_btn);
        close_btn.setOnClickListener(this);
        ListView listview;
        hydrationAdapter adapter;

        // Adapter 생성
        adapter = new hydrationAdapter();

        // 리스트뷰 참조 및 Adapter달기
        listview = (ListView) findViewById(R.id.hydration_listview);
        listview.setAdapter(adapter);

        // 첫 번째 아이템 추가.
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.a), "가격");
        // 두 번째 아이템 추가.
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.b), "가깝다");
        // 세 번째 아이템 추가.
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.c), "가다듬다1");
        // 네 번쨰 아이템 추가.
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.d), "가다듬다2");
        // 다섯 번째 아이템 추가.
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.e), "갈림길");
        // 여섯 번째 아이템 추가.
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.f), "감기1");
        // 일곱 번째 아이템 추가.
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.g), "감기2");
        // 여덟 번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.h), "걱정");
        // 아홉 번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.i), "건강");
        // 열 번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.j), "그것");
        // 열 한번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.k), "길");
        // 열 두번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.l), "돕다");
        // 열 세번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.m), "만나다");
        // 열 네번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.n), "사다");
        // 열 다섯번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.o), "사랑");
        // 열 여섯번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.p), "시청1");
        // 열 일곱번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.q), "시청2");
        // 열 여덜번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.r), "안녕1");
        // 열 아홉번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.s), "안녕2");
        // 스무 번째 아이템추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.t), "안녕3");
        // 스물 한번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.u), "안녕하십니까1");
        // 스물 두번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.v), "안녕하십니까2");
        // 스물 세번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.w), "안녕하십니까3");
        // 스물 네번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.x), "이것");
        // 스물 다섯번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.y), "저것");
        // 스물 여섯번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.z), "주다");
        // 스물 일곱번째 아이템 추가
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.z1), "팔다");


        listview.setAdapter(adapter);
        listview.invalidate();

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                // get item
                hydration_data item = (hydration_data) parent.getItemAtPosition(position);

                String titleStr = item.getTitle();
                Drawable iconDrawable = item.getIcon();

                Log.d("position", "" + position);
                Log.d("title", "" + titleStr);


                Intent in = new Intent(HydrationActivity.this, hydration_Read.class);
                in.putExtra("title", titleStr);
                in.putExtra("pos",""+position);
                startActivity(in);

                // TODO : use item data.
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.close_btn:
                finish();
                break;
        }
    }
}
