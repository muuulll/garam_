package com.example.Garam_;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import java.lang.Math;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.example.Garam_.AlramActivity.search_m;

public class ServiceThread extends Thread {
    Handler handler;
    String phonenum, lat, lng, meterset;
    boolean isRun = true;
    String meter = "0";

    public ServiceThread(Handler handler, String phonenum, String meterset) {
        this.handler = handler;
        this.phonenum = phonenum;
        this.meterset = meterset;
    }

    public void stopForever() {
        synchronized (this) {
            this.isRun = false;
        }
    }

    public void run() {
        //반복적으로 수행할 작업을 한다.
        while (isRun) {

            GetgpsAsyncTask getgpsAsyncTask = new GetgpsAsyncTask();
            getgpsAsyncTask.execute();
            Log.d("meter _set", meter + " " + meterset);
            if (Integer.parseInt(meter) >= Integer.parseInt(meterset))
                handler.sendEmptyMessage(0);//쓰레드에 있는 핸들러에게 메세지를 보냄
            try {
                Thread.sleep(10000); //10초씩 쉰다.
            } catch (Exception e) {
            }

        }
    }

    public class GetgpsAsyncTask extends AsyncTask<Void, Void, Void> {
        String parser_txt;
        boolean result_state = false;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {

            postData();
            return null;
        }

        protected void onPostExecute(Void content) {
            MyloactionActivty myloactionActivty = new MyloactionActivty();
            myloactionActivty.lat();
            myloactionActivty.lng();
            Log.d("setD", myloactionActivty.lat() + " " + myloactionActivty.lng() + " " + lat + " " + lng);
            double distanceMeter =
                    distance(myloactionActivty.lat(), myloactionActivty.lng(), Double.parseDouble(lat), Double.parseDouble(lng), "meter");
            meter = Integer.toString((int) distanceMeter);
            Log.d("meter", meter);
            search_m.setText("상대방과\n" + meter + "M\n거리입니다");
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        public void postData() {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://cnscap.dothome.co.kr/get_gps.php");
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();


                nameValuePairs.add(new BasicNameValuePair("person_id", phonenum));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                parser_txt = EntityUtils.toString(response.getEntity());
                if (parser_txt.equalsIgnoreCase("") || parser_txt == null) {
                    result_state = false;
                } else {
                    result_state = true;
                    Log.d("response", parser_txt);
                    lat = parser_txt.split(",")[1];
                    lng = parser_txt.split(",")[2];
                }

            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }
        }
    }

    /**
     * 두 지점간의 거리 계산
     *
     * @param lat1 지점 1 위도
     * @param lon1 지점 1 경도
     * @param lat2 지점 2 위도
     * @param lon2 지점 2 경도
     * @param unit 거리 표출단위
     * @return
     */
    private static double distance(double lat1, double lon1, double lat2, double lon2, String unit) {

        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));

        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;

        if (unit == "kilometer") {
            dist = dist * 1.609344;
        } else if (unit == "meter") {
            dist = dist * 1609.344;
        }

        return (dist);
    }


    // This function converts decimal degrees to radians
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    // This function converts radians to decimal degrees
    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }
}