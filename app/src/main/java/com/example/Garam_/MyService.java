package com.example.Garam_;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.widget.TextView;
import android.widget.Toast;

public class MyService extends Service {
    NotificationManager Notifi_M;
    ServiceThread thread;
    Notification Notifi;
    String phonenum,meterset;

    @Override
    public IBinder onBind(Intent intent) {
        phonenum = intent.getStringExtra("phonenum");
        meterset= intent.getStringExtra("meterset");
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Notifi_M = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        myServiceHandler handler = new myServiceHandler();
        phonenum = intent.getStringExtra("phonenum");
        meterset= intent.getStringExtra("meterset");
        thread = new ServiceThread(handler,phonenum,meterset);
        thread.start();
        return START_STICKY;
    }

    //서비스가 종료될 때 할 작업

    public void onDestroy() {
        thread.stopForever();
        thread = null;//쓰레기 값을 만들어서 빠르게 회수하라고 null을 넣어줌.
    }

    class myServiceHandler extends Handler {
        @Override
        public void handleMessage(android.os.Message msg) {
            Intent intent = new Intent(MyService.this, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(MyService.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            Notifi = new Notification.Builder(getApplicationContext())
                    .setContentTitle("거리가 떨어졌습니다.")
                    .setContentText("앱에서 거리를 확인하세요.")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setTicker("알림!!!")
                    .setContentIntent(pendingIntent)
                    .build();

            //소리 진동 추가

            if (MainActivity.sound_alram.isChecked() && MainActivity.vibe_alram.isChecked() )
                Notifi.defaults = Notification.DEFAULT_ALL;
            else if(MainActivity.sound_alram.isChecked())
                Notifi.defaults = Notification.DEFAULT_SOUND;
            else if(MainActivity.vibe_alram.isChecked())
                Notifi.defaults = Notification.DEFAULT_VIBRATE;
            else if(MainActivity.no_alram.isChecked())
                Notifi.defaults = Notification.DEFAULT_LIGHTS;
            //알림 소리를 한번만 내도록
            Notifi.flags = Notification.FLAG_ONLY_ALERT_ONCE;

            //확인하면 자동으로 알림이 제거 되도록
            Notifi.flags = Notification.FLAG_AUTO_CANCEL;


            Notifi_M.notify(777, Notifi);

            //토스트 띄우기
            Toast.makeText(MyService.this, "거리가 멀어졌습니다.", Toast.LENGTH_LONG).show();
        }
    }

    ;
}