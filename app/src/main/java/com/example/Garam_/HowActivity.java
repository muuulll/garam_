package com.example.Garam_;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
/*
사용방법 Activity
*/
public class HowActivity extends Activity implements View.OnClickListener {
    ImageButton close_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //메모리가 부족하거나 해서 kill될시에 자동으로 복구 시켜준다.(savedInstanceState)
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how);

        //close 버튼
        close_btn = (ImageButton) findViewById(R.id.close_btn);
        //Close클릭 리스너
        close_btn.setOnClickListener(this);

    }

    @Override
    //close버튼 클릭시 종료
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.close_btn:
                finish();
                break;
        }
    }
}
