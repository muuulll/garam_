package com.example.Garam_;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageButton;

import java.util.ArrayList;
/*
공지사항 Activity
*/
public class NoticeActivity extends Activity implements View.OnClickListener {
    ExpandableListView notice_list;
    ImageButton close_btn;
    private ArrayList<String> mGroupList = null;
    private ArrayList<String> mChildList = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);

        notice_list = (ExpandableListView) findViewById(R.id.notice_list);
        close_btn =(ImageButton)findViewById(R.id.close_btn);
        close_btn.setOnClickListener(this);

        // 공지사항 저장할 리스트 만듬
        mGroupList = new ArrayList<String>();
        mChildList = new ArrayList<String>();

        //큰 제목 집어넣음
        mGroupList.add("※공지사항 - 사용법 설명");
        mGroupList.add("※공지사항 - 수화 교육");
        mGroupList.add("※공지사항 - 고객센터");

        //큰제목 누르면 나오는 내용들
        mChildList.add("1");
        mChildList.add("2");
        mChildList.add("3");

        //큰제목과 내용을 어댑터로 넘김
        notice_list.setAdapter(new BaseExpandableAdapter(this, mGroupList, mChildList));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.close_btn:
                finish();
                break;
        }
    }
}
