package com.example.Garam_;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/*
알람 Activity
*/

public class AlramActivity extends Activity implements View.OnClickListener {
    ImageButton close_btn;
   static TextView search_m;
    Button on_btn, off_btn;

    @Override

    //process 가 메모리 부족등으로 kill 된 후에 다시 복원 될 때, savedInstanceState 정보를 가지고 자동으로 복원해 준다.
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alram);

        //findView로 xml과 연결 클로스버튼 아이디 불러오기
        close_btn = (ImageButton) findViewById(R.id.close_btn);
        //close클릭 리스너
        close_btn.setOnClickListener(this);

        //상대방과 거리 확인 텍스트뷰
        search_m = (TextView) findViewById(R.id.search_m);
        //서비스 on버튼
        on_btn = (Button) findViewById(R.id.on_btn);
        //서비스 off버튼
        off_btn = (Button) findViewById(R.id.off_btn);

        //서비스 on클릭 리스너
        on_btn.setOnClickListener(this);
        //서비스 off클릭 리스너
        off_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            //액티비티 종료
            case R.id.close_btn:
                finish();
                break;
            case R.id.on_btn:
                //메인에서 폰번호 등록 안됬으면 체크
                if (getIntent().getStringExtra("phonenum") != null) {
                    //메인에서 폰번호 등록 안됬으면 체크
                    if (!getIntent().getStringExtra("phonenum").equalsIgnoreCase("")) {
                        //서비스 버튼 배경 및 텍스트 컬러 지정
                        search_m.setTextColor(Color.BLACK);
                        off_btn.setTextColor(Color.GRAY);
                        on_btn.setTextColor(Color.BLACK);
                        off_btn.setBackgroundColor(Color.argb(255, 0, 121, 107));
                        on_btn.setBackgroundColor(Color.argb(255, 224, 242, 241));

                        //서비스 시작 팝업창 생성
                        Toast.makeText(getApplicationContext(), "Service 시작", Toast.LENGTH_SHORT).show();

                        // intent 활용 MyService 객체를 참조
                        intent = new Intent(this, MyService.class);
                        //서비스로 연결할 기기번호랑 거리 넘김
                        intent.putExtra("phonenum", getIntent().getStringExtra("phonenum"));
                        intent.putExtra("meterset", getIntent().getStringExtra("meterset"));
                        Log.d("aa", getIntent().getStringExtra("meterset") + "");
                        startService(intent);
                    }
                    //폰 번호가 등록 안됫을 경우
                    else {
                        Toast.makeText(this, "메인에서 기기를 적용하고 오세요.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "메인에서 기기를 적용하고 오세요.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.off_btn:
                //서비스 종료
                search_m.setTextColor(Color.GRAY);
                off_btn.setTextColor(Color.BLACK);
                on_btn.setTextColor(Color.GRAY);
                on_btn.setBackgroundColor(Color.argb(255, 0, 121, 107));
                off_btn.setBackgroundColor(Color.argb(255, 224, 242, 241));
                Toast.makeText(getApplicationContext(), "Service 끝", Toast.LENGTH_SHORT).show();
                intent = new Intent(this, MyService.class);
                stopService(intent);
                break;
        }
    }
}
