package com.example.Garam_;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
/*
공지사항 리스트뷰 아이템 어댑터
*/
public class BaseExpandableAdapter extends BaseExpandableListAdapter {

    //
    private ArrayList<String> groupList = null;
    private ArrayList<String> childList = null;
    private LayoutInflater inflater = null;
    private ViewHolder viewHolder = null;

    //공지사항 어댑터
    public BaseExpandableAdapter(Context c, ArrayList<String> groupList,
                                 ArrayList<String> childList){
        super();
        //인플레이터는 xml파일을 통해서 객체화 시킨다. 여기서는 Context를 객체화...
        this.inflater = LayoutInflater.from(c);
        //공지사항 큰 제목 받아옴
        this.groupList = groupList;
        //큰 제목 누르면 나오는 내용들
        this.childList = childList;
    }

    // 그룹 포지션을 반환한다.
    @Override
    public String getGroup(int groupPosition) {
        return groupList.get(groupPosition);
    }

    // 그룹 사이즈를 반환한다.
    @Override
    public int getGroupCount() {
        return groupList.size();
    }

    // 그룹 ID를 반환한다.
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    // 그룹뷰 각각의 ROW
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        View v = convertView;

        if(v == null){
            viewHolder = new ViewHolder();
            //레이아웃 가져오기
            v = inflater.inflate(R.layout.list_row, parent, false);
            //큰 제목 텍스트뷰
            viewHolder.tv_groupName = (TextView) v.findViewById(R.id.tv_group);
            v.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)v.getTag();
       }
        viewHolder.tv_groupName.setText(getGroup(groupPosition));

        return v;
    }

    // 차일드뷰를 반환한다.
    @Override
    public String getChild(int groupPosition, int childPosition) {
        return childList.get(groupPosition);
    }

    // 차일드뷰 사이즈를 반환한다.
    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    // 차일드뷰 ID를 반환한다.
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    // 차일드뷰 각각의 ROW
    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        View v = convertView;

        if(v == null){
            viewHolder = new ViewHolder();
            v = inflater.inflate(R.layout.list_row, null);
            viewHolder.tv_childName = (TextView) v.findViewById(R.id.tv_child);
            v.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)v.getTag();
        }

        viewHolder.tv_childName.setText(getChild(groupPosition, childPosition));

        return v;
    }

    @Override
    public boolean hasStableIds() { return true; }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) { return true; }

    class ViewHolder {
        public TextView tv_groupName;
        public TextView tv_childName;
    }

}