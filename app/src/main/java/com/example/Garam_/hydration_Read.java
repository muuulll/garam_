package com.example.Garam_;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

/*
수화 설명 Activity
*/
public class hydration_Read extends Activity implements View.OnClickListener {

    TextView hydration_read_text;
    ImageView hydration_read_img;
    String pos;
ImageButton close_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hydration_read);

        close_btn = (ImageButton)findViewById(R.id.close_btn);
        close_btn.setOnClickListener(this);
        hydration_read_img = (ImageView) findViewById(R.id.hydration_read_img);
        hydration_read_text = (TextView) findViewById(R.id.hydration_read_text

        );

        Intent in = getIntent();


        //hydration_read_text.setText(in.getStringExtra("title"));

        pos = in.getStringExtra("pos");

        //Log.d("pos",""+pos);
        if (in.getStringExtra("pos").equals("0")) {
            Glide.with(getApplicationContext()).load(R.drawable.a).into(hydration_read_img);
            hydration_read_text.setText("물건의 가치를 돈으로 나타낸 것.\n" +
                    "¶ 판매 가격을 협정하다.\n" +
                    "오른손의 1·5지 끝을 맞대어 동그라미를 만들어 좌우로 가볍게 두 번 흔든다.");
        } else if (in.getStringExtra("pos").equals("1")) {
            Glide.with(getApplicationContext()).load(R.drawable.b).into(hydration_read_img);
            hydration_read_text.setText("어느 한 곳에서 다른 곳까지의 거리가 짧다.\n" +
                    "¶ 학교에 가까운 곳에 살다.\n" +
                    "[짧은 두 지점을 나타내는 동작]\n" +
                    "1·5지 끝을 맞대어 동그라미를 만든 두 주먹을 모로 세워 전후에서 접근시켜 맞댄다.");
        } else if (in.getStringExtra("pos").equals("2")) {
            Glide.with(getApplicationContext()).load(R.drawable.c).into(hydration_read_img);
            hydration_read_text.setText("정신, 생각, 마음 따위를 바로 잡거나 정리하다.\n" +
                    "¶ 생각을 가다듬다.\n" +
                    "[생각+다시 일어섬을 나타내는 동작]\n" +
                    "오른 주먹의 1지를 펴서 끝을 오른쪽 관자놀이에 댄 다음, 두 주먹을 오른쪽으로 약간 눕혔다가 바로 세우며 두 주먹의 1지를 확 편다.");

        } else if (in.getStringExtra("pos").equals("3")) {
            Glide.with(getApplicationContext()).load(R.drawable.d).into(hydration_read_img);
            hydration_read_text.setText("정신, 생각, 마음 따위를 바로 잡거나 정리하다.\n" +
                    "¶ 생각을 가다듬다.\n" +
                    "[생각+다시 일어섬을 나타내는 동작]\n" +
                    "오른 주먹의 1지를 펴서 끝을 오른쪽 관자놀이에 댄 다음, 두 주먹을 오른쪽으로 약간 눕혔다가 바로 세우며 두 주먹의 1지를 확 편다.");
        } else if (in.getStringExtra("pos").equals("4")) {
            Glide.with(getApplicationContext()).load(R.drawable.e).into(hydration_read_img);
            hydration_read_text.setText("한 갈래의 길이 두 갈래로 갈라져서 난 길.\n" +
                    "¶ 갈림길에 서다.\n" +
                    "[두 방향으로 갈라지는 동작]\n" +
                    "두 손바닥을 마주 보게 하였다가 내밀며 벌린다.");
        } else if (in.getStringExtra("pos").equals("5")) {
            Glide.with(getApplicationContext()).load(R.drawable.f).into(hydration_read_img);
            hydration_read_text.setText("주로 바이러스로 말미암아 걸리는 호흡기 계통의 병.\n" +
                    "¶ 감기에 걸리다.\n" +
                    "[① 뱉어내는 동작 <기침>. ② 콧물이 내려옴을 나타내는 동작]\n" +
                    "① 오른 손의 1·5지 끝을 맞대어 동그라미를 만들어 턱밑 가까이 댔다가 입을 벌리며 내민다.");
        } else if (in.getStringExtra("pos").equals("6")) {
            Glide.with(getApplicationContext()).load(R.drawable.g).into(hydration_read_img);
            hydration_read_text.setText("주로 바이러스로 말미암아 걸리는 호흡기 계통의 병.\n" +
                    "¶ 감기에 걸리다.\n" +
                    "[① 뱉어내는 동작 <기침>. ② 콧물이 내려옴을 나타내는 동작]\n" +
                    "① 오른 손의 1·5지 끝을 맞대어 동그라미를 만들어 턱밑 가까이 댔다가 입을 벌리며 내민다.");
        } else if (in.getStringExtra("pos").equals("7")) {
            Glide.with(getApplicationContext()).load(R.drawable.h).into(hydration_read_img);
            hydration_read_text.setText("안심이 되지 않아 속을 태움.\n" +
                    "¶ 빚을 갚지 못해 걱정이다.\n" +
                    "[가슴 속이 들끓는 동작]\n" +
                    "약간 구부린 오른 손끝을 가슴에 대고 왼쪽으로 한 바퀴 돌린다.");
        } else if (in.getStringExtra("pos").equals("8")) {
            Glide.with(getApplicationContext()).load(R.drawable.i).into(hydration_read_img);
            hydration_read_text.setText("정신적으로나 육체적으로 아무 탈이 없고 튼튼함.\n" +
                    "¶ 누구나 건강에 관심이 많다.\n" +
                    "[힘을 과시하는 동작]\n" +
                    "두 주먹을 가슴 앞에서 힘주어 당긴다.\n");
        } else if (in.getStringExtra("pos").equals("9")) {
            Glide.with(getApplicationContext()).load(R.drawable.j).into(hydration_read_img);
            hydration_read_text.setText("듣는 이에게 가까이 있거나 듣는 이가 생각하고 있는 사물을 가리키는 지시 대명사.\n" +
                    "¶ 그것을 보아라.\n" +
                    "오른 주먹의 1지를 펴서 끝이 밖으로 향하게 하여 약간 내민다.");
        } else if (in.getStringExtra("pos").equals("10")) {
            Glide.with(getApplicationContext()).load(R.drawable.k).into(hydration_read_img);
            hydration_read_text.setText("사람이나 동물 또는 자동차 따위가 지나갈 수 있게 땅 위에 낸 일정한 너비의 공간.\n" +
                    "¶ 길을 건너다.\n" +
                    "[길을 내는 동작]\n" +
                    "손끝이 밖으로 향하게 모로 세운 두 손을 마주 보게 하여 좌우로 움직이며 내민다.");
        } else if (in.getStringExtra("pos").equals("11")) {
            Glide.with(getApplicationContext()).load(R.drawable.l).into(hydration_read_img);
            hydration_read_text.setText("남이 하는 일이 잘되도록 거들거나 힘을 보태다.\n" +
                    "¶ 어려운 이웃을 돕다.\n" +
                    "[밀어주는 동작]\n" +
                    "왼 주먹의 5지를 펴서 바닥이 밖으로 향하게 세우고, 그 등에 오른 손바닥을 세워 두 번 댄다.");
        } else if (in.getStringExtra("pos").equals("12")) {
            Glide.with(getApplicationContext()).load(R.drawable.m).into(hydration_read_img);
            hydration_read_text.setText("누군가 가거나 와서 둘이 서로 마주 보다.\n" +
                    "¶ 선생님을 만나다.\n" +
                    "[가까이 이르러 마주하는 동작]\n" +
                    "두 주먹의 1지를 펴서 마주 세웠다가 중앙으로 모아 마주 댄다.");
        } else if (in.getStringExtra("pos").equals("13")) {
            Glide.with(getApplicationContext()).load(R.drawable.n).into(hydration_read_img);
            hydration_read_text.setText("값을 치르고 어떤 물건이나 권리를 자기 것으로 만들다.\n" +
                    "¶ 책을 사다.\n" +
                    "[값을 치르는 것을 나타내는 동작]\n" +
                    "1·5지 끝을 맞대어 동그라미를 만든 오른손을, 손등이 위로 향하게 편 왼 손목 위에서 밖으로 내밀며 편다.");
        } else if (in.getStringExtra("pos").equals("14")) {
            Glide.with(getApplicationContext()).load(R.drawable.o).into(hydration_read_img);
            hydration_read_text.setText("이성의 상대에게 끌려 열렬히 좋아하는 마음.\n" +
                    "¶ 사랑을 고백하다.\n" +
                    "[어루만지는 동작]\n" +
                    "5지 바닥을 1지 옆면에 대고 손등이 왼쪽으로 향하게 모로 세운 왼 주먹 위에 오른 손바닥을 대고 오른손만 오른쪽으로 돌린다.");
        } else if (in.getStringExtra("pos").equals("15")) {
            Glide.with(getApplicationContext()).load(R.drawable.p).into(hydration_read_img);
            hydration_read_text.setText("시의 행정 사무를 맡아보는 기관.\n" +
                    "[시+책임]\n" +
                    "손끝이 밖으로 향하게 펴서 모로 세운 두 손을 맞댔다가 오른손만 위로 올린 다음, 두 손을 약간 구부려 양 어깨에 올려놓는다.");
        } else if (in.getStringExtra("pos").equals("16")) {
            Glide.with(getApplicationContext()).load(R.drawable.q).into(hydration_read_img);
            hydration_read_text.setText("시의 행정 사무를 맡아보는 기관.\n" +
                    "[시+책임]\n" +
                    "손끝이 밖으로 향하게 펴서 모로 세운 두 손을 맞댔다가 오른손만 위로 올린 다음, 두 손을 약간 구부려 양 어깨에 올려놓는다.");
        } else if (in.getStringExtra("pos").equals("17")) {
            Glide.with(getApplicationContext()).load(R.drawable.r).into(hydration_read_img);
            hydration_read_text.setText("¶ 안녕, 내일 아침에 또 만나자.\n" +
                    "① 오른 손바닥으로 주먹을 쥔 왼 팔을 쓸어내린 다음, 두 주먹을 쥐고 바닥이 아래로 향하게 하여 가슴 앞에서 아래로 내린다. ② 두 주먹을 쥐고 바닥이 아래로 향하게 하여 가슴 앞에서 아래로 내린다.");
        } else if (in.getStringExtra("pos").equals("18")) {
            Glide.with(getApplicationContext()).load(R.drawable.s).into(hydration_read_img);
            hydration_read_text.setText("¶ 안녕, 내일 아침에 또 만나자.\n" +
                    "① 오른 손바닥으로 주먹을 쥔 왼 팔을 쓸어내린 다음, 두 주먹을 쥐고 바닥이 아래로 향하게 하여 가슴 앞에서 아래로 내린다. ② 두 주먹을 쥐고 바닥이 아래로 향하게 하여 가슴 앞에서 아래로 내린다.");
        } else if (in.getStringExtra("pos").equals("19")) {
            Glide.with(getApplicationContext()).load(R.drawable.t).into(hydration_read_img);
            hydration_read_text.setText("¶ 안녕, 내일 아침에 또 만나자.\n" +
                    "① 오른 손바닥으로 주먹을 쥔 왼 팔을 쓸어내린 다음, 두 주먹을 쥐고 바닥이 아래로 향하게 하여 가슴 앞에서 아래로 내린다. ② 두 주먹을 쥐고 바닥이 아래로 향하게 하여 가슴 앞에서 아래로 내린다.");
        } else if (in.getStringExtra("pos").equals("20")) {
            Glide.with(getApplicationContext()).load(R.drawable.u).into(hydration_read_img);
            hydration_read_text.setText("① 오른 손바닥으로 주먹을 쥔 왼 팔을 쓸어내린 다음, 두 주먹을 쥐고 바닥이 아래로 향하게 하여 가슴 앞에서 아래로 내린다.\n" +
                    " ② 두 주먹을 쥐고 바닥이 아래로 향하게 하여 가슴 앞에서 아래로 내린다.");
        } else if (in.getStringExtra("pos").equals("21")) {
            Glide.with(getApplicationContext()).load(R.drawable.v).into(hydration_read_img);
            hydration_read_text.setText("① 오른 손바닥으로 주먹을 쥔 왼 팔을 쓸어내린 다음, 두 주먹을 쥐고 바닥이 아래로 향하게 하여 가슴 앞에서 아래로 내린다.\n" +
                    " ② 두 주먹을 쥐고 바닥이 아래로 향하게 하여 가슴 앞에서 아래로 내린다.");
        } else if (in.getStringExtra("pos").equals("22")) {
            Glide.with(getApplicationContext()).load(R.drawable.w).into(hydration_read_img);
            hydration_read_text.setText("① 오른 손바닥으로 주먹을 쥔 왼 팔을 쓸어내린 다음, 두 주먹을 쥐고 바닥이 아래로 향하게 하여 가슴 앞에서 아래로 내린다.\n" +
                    " ② 두 주먹을 쥐고 바닥이 아래로 향하게 하여 가슴 앞에서 아래로 내린다.");
        } else if (in.getStringExtra("pos").equals("23")) {
            Glide.with(getApplicationContext()).load(R.drawable.x).into(hydration_read_img);
            hydration_read_text.setText("가까운 자리에 있는 일이나 물건을 가리키는 말.\n" +
                    "¶ 이것은 연필이다.\n" +
                    "오른 주먹의 1지를 펴서 끝이 약간 아래로 향하게 한다.");
        } else if (in.getStringExtra("pos").equals("24")) {
            Glide.with(getApplicationContext()).load(R.drawable.y).into(hydration_read_img);
            hydration_read_text.setText("저기에 있는 사물을 가리키는 말.\n" +
                    "¶ 저것을 보세요.\n" +
                    "오른 주먹의 1지를 펴서 끝이 밖으로 향하게 한다.");
        } else if (in.getStringExtra("pos").equals("25")) {
            Glide.with(getApplicationContext()).load(R.drawable.z).into(hydration_read_img);
            hydration_read_text.setText("물건 따위를 남에게 건네어 가지거나 누리게 하다.\n" +
                    "¶ 개에게 먹이를 준다.\n" +
                    "[손에 받쳐든 것을 내어 미는 동작]\n" +
                    "오른손을 펴서 손바닥이 위로 손끝이 밖으로 향하게 하여 밖으로 내민다.");
        } else if (in.getStringExtra("pos").equals("26")) {
            Glide.with(getApplicationContext()).load(R.drawable.z1).into(hydration_read_img);
            hydration_read_text.setText("값을 받고 물건이나 권리 따위를 남에게 넘기거나 노력 따위를 제공하다.\n" +
                    "¶ 소를 판다.\n" +
                    "[묶음에서 일부를 빼내는 동작]\n" +
                    "오른 주먹의 1·2지를 펴서 바닥을 왼 손바닥에 대고 왼손으로 감싸 쥔 다음, 오른손의 1·2지를 밑으로 빼낸다.");
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.close_btn:
                finish();
                break;
        }
    }
}
