package com.example.Garam_;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
/*
음성인식 Activity
*/
public class VoicescannActivity extends Activity implements View.OnClickListener {
    LinearLayout st_voice_lin;
    EditText get_voice;
    ImageButton close_btn;
    private static final int RESULT_SPPECH = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voicescann);

        st_voice_lin = (LinearLayout)findViewById(R.id.st_voice_lin);
        st_voice_lin.setOnClickListener(this);
        get_voice =(EditText)findViewById(R.id.get_voice);
        close_btn = (ImageButton)findViewById(R.id.close_btn);
        close_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch(view.getId()){
            case R.id.close_btn:
                finish();
                break;
            case R.id.st_voice_lin:
                intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getPackageName());
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ko-KR");
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "말해주세요");

                Toast.makeText(this, "start speak", Toast.LENGTH_SHORT).show();


                try {
                    startActivityForResult(intent, RESULT_SPPECH);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getApplicationContext(), "stt 지원안함", Toast.LENGTH_SHORT).show();
                    e.getStackTrace();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && (requestCode == RESULT_SPPECH)) {
            ArrayList<String> sstResult = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String result_sst = sstResult.get(0);
            get_voice.setText("" + result_sst);
        }
    }
}
