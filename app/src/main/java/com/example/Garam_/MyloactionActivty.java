package com.example.Garam_;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nhn.android.maps.NMapActivity;
import com.nhn.android.maps.NMapCompassManager;
import com.nhn.android.maps.NMapController;
import com.nhn.android.maps.NMapLocationManager;
import com.nhn.android.maps.NMapOverlay;
import com.nhn.android.maps.NMapOverlayItem;
import com.nhn.android.maps.NMapView;
import com.nhn.android.maps.maplib.NGeoPoint;
import com.nhn.android.maps.nmapmodel.NMapError;
import com.nhn.android.maps.overlay.NMapPOIdata;
import com.nhn.android.mapviewer.overlay.NMapCalloutOverlay;
import com.nhn.android.mapviewer.overlay.NMapMyLocationOverlay;
import com.nhn.android.mapviewer.overlay.NMapOverlayManager;
import com.nhn.android.mapviewer.overlay.NMapPOIdataOverlay;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
나의 위치 Activity
*/
public class MyloactionActivty extends NMapActivity implements NMapView.OnMapStateChangeListener, NMapView.OnMapViewTouchEventListener, NMapOverlayManager.OnCalloutOverlayListener, View.OnClickListener, NMapLocationManager.OnLocationChangeListener {
    NMapController mMapController;
    RelativeLayout map_view;
    ImageButton close_btn;
    Button search_my_location;
    TextView title_name;
    public static double lat = 35.8143046, lon = 127.0922283;
    private NMapLocationManager mMapLocationManager;
    private NMapCompassManager mMapCompassManager;
    private NMapMyLocationOverlay mMyLocationOverlay;
    private NMapOverlayManager mOverlayManager;
    private MapContainerView mMapContainerView;
    NMapViewerResourceProvider mMapViewerResourceProvider;
    NMapView mMapView;
    String set_phonenum;

    public double lat() {
        return lat;
    }

    public double lng() {
        return lon;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        //현재위치 찾는 버튼
        search_my_location = (Button) findViewById(R.id.search_my_location);
        search_my_location.setOnClickListener(this);

        //메인에서 설정한 기기 번호를 가져옴
        set_phonenum = getIntent().getStringExtra("phonenum");
        //타이틀 제목 객체 생성
        title_name = (TextView) findViewById(R.id.title_name);
        title_name.setText("나의 위치");

        //닫기버튼 생성
        close_btn = (ImageButton) findViewById(R.id.close_btn);
        close_btn.setOnClickListener(this);
        //지도 화면 생성
        map_view = (RelativeLayout) findViewById(R.id.reMap);

        //지도 객체 생성
        mMapView = new NMapView(this);
        //네이버 지도 api키
        mMapView.setClientId("dIw8TF1yuQXIZzu7v3I0");
        //지도 객체에 지도 집어넣음
        map_view.addView(mMapView);
        //클릭 드래그 활성화
        mMapView.setClickable(true);

        //연결할 기기 번호가 있으면
        if (set_phonenum != null && !set_phonenum.equalsIgnoreCase("")) {
            //기기의 위도 경도를 가져오는 함수 실행
            GetgpsAsyncTask getgpsAsyncTask = new GetgpsAsyncTask();
            getgpsAsyncTask.execute();
        }

        //맵정보 변경 리스너
        mMapView.setOnMapStateChangeListener(this);
        //맵 터치 리스너
        mMapView.setOnMapViewTouchEventListener(this);

        mMapController = mMapView.getMapController();
        mMapViewerResourceProvider = new NMapViewerResourceProvider(this);
        //오버레이 메니저 객채 생성
        mOverlayManager = new NMapOverlayManager(this, mMapView, mMapViewerResourceProvider);
        //이 아래로는 현재 위치 찾는 함수들
        // location manager
        mMapLocationManager = new NMapLocationManager(this);
        mMapLocationManager.setOnLocationChangeListener(onMyLocationChangeListener);

        // compass manager
        mMapCompassManager = new NMapCompassManager(this);

        // create my location overlay
        //현재위치를 지도에 표시
        mMyLocationOverlay = mOverlayManager.createMyLocationOverlay(mMapLocationManager, mMapCompassManager);
    }

    private final NMapLocationManager.OnLocationChangeListener onMyLocationChangeListener = new NMapLocationManager.OnLocationChangeListener() {

        @Override
        public boolean onLocationChanged(NMapLocationManager locationManager, NGeoPoint myLocation) {

            if (mMapController != null) {
                mMapController.animateTo(myLocation);
                //내위치 찾으면 저장
                lat = myLocation.getLatitude();
                lon = myLocation.getLongitude();
                //서버로 내 위치 전달
                GpsAsyncTask gpsAsyncTask = new GpsAsyncTask();
                gpsAsyncTask.execute();
            }

            return true;
        }

        @Override
        public void onLocationUpdateTimeout(NMapLocationManager locationManager) {
            Toast.makeText(MyloactionActivty.this, "Your current location is temporarily unavailable.", Toast.LENGTH_LONG).show();
            stopMyLocation();
        }

        @Override
        public void onLocationUnavailableArea(NMapLocationManager locationManager, NGeoPoint myLocation) {
            Toast.makeText(MyloactionActivty.this, "Your current location is unavailable area.", Toast.LENGTH_LONG).show();
            //내위치 찾기 종료
            stopMyLocation();
        }
    };

    //위치찾기 종료 함수
    private void stopMyLocation() {
        state = false;
        if (mMyLocationOverlay != null) {
            mMapLocationManager.disableMyLocation();

            if (mMapView.isAutoRotateEnabled()) {
                mMyLocationOverlay.setCompassHeadingVisible(false);

                mMapCompassManager.disableCompass();

                mMapView.setAutoRotateEnabled(false, false);

                mMapContainerView.requestLayout();
            }
        }
    }

    @Override
    public void onLongPress(NMapView nMapView, MotionEvent motionEvent) {

    }

    @Override
    public void onLongPressCanceled(NMapView nMapView) {

    }

    @Override
    public void onTouchDown(NMapView nMapView, MotionEvent motionEvent) {

    }

    @Override
    public void onTouchUp(NMapView nMapView, MotionEvent motionEvent) {

    }

    @Override
    public void onScroll(NMapView nMapView, MotionEvent motionEvent, MotionEvent motionEvent1) {

    }

    @Override
    public void onSingleTapUp(NMapView nMapView, MotionEvent motionEvent) {

    }

    @Override
    public void onMapInitHandler(NMapView nMapView, NMapError nMapError) {
        if (nMapError == null) { // success
            //저장되어 있는 최근 위도 경도로 처음 지도를 띄울때 바로 화면이 나오게 설정
            mMapController.setMapCenter(new NGeoPoint(lon, lat), 11);
        } else { // fail
            Log.e("1", "onMapInitHandler: error=" + nMapError.toString());
        }
    }

    @Override
    public void onMapCenterChange(NMapView nMapView, NGeoPoint nGeoPoint) {

    }

    @Override
    public void onMapCenterChangeFine(NMapView nMapView) {

    }

    @Override
    public void onZoomLevelChange(NMapView nMapView, int i) {

    }

    @Override
    public void onAnimationStateChange(NMapView nMapView, int i, int i1) {

    }

    @Override
    public NMapCalloutOverlay onCreateCalloutOverlay(NMapOverlay nMapOverlay, NMapOverlayItem nMapOverlayItem, Rect rect) {
        return null;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.search_my_location:
                if (state == false) {
                    //현재위치 찾기
                    startMyLocation();
                } else {
                    //찾기가 실행중이면 종료
                    stopMyLocation();
                }
                break;
            case R.id.close_btn:
                finish();
                break;
        }
    }

    @Override
    public boolean onLocationChanged(NMapLocationManager nMapLocationManager, NGeoPoint nGeoPoint) {
        return false;
    }

    @Override
    public void onLocationUpdateTimeout(NMapLocationManager nMapLocationManager) {

    }

    @Override
    public void onLocationUnavailableArea(NMapLocationManager nMapLocationManager, NGeoPoint nGeoPoint) {

    }

    //지도에서 내위치 찾는 네이버 지도 라이브러리
    private class MapContainerView extends ViewGroup {

        public MapContainerView(Context context) {
            super(context);
        }

        @Override
        protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
            final int width = getWidth();
            final int height = getHeight();
            final int count = getChildCount();
            for (int i = 0; i < count; i++) {
                final View view = getChildAt(i);
                final int childWidth = view.getMeasuredWidth();
                final int childHeight = view.getMeasuredHeight();
                final int childLeft = (width - childWidth) / 2;
                final int childTop = (height - childHeight) / 2;
                view.layout(childLeft, childTop, childLeft + childWidth, childTop + childHeight);
            }

            if (changed) {
                mOverlayManager.onSizeChanged(width, height);
            }
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            int w = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
            int h = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
            int sizeSpecWidth = widthMeasureSpec;
            int sizeSpecHeight = heightMeasureSpec;

            final int count = getChildCount();
            for (int i = 0; i < count; i++) {
                final View view = getChildAt(i);

                if (view instanceof NMapView) {
                    if (mMapView.isAutoRotateEnabled()) {
                        int diag = (((int) (Math.sqrt(w * w + h * h)) + 1) / 2 * 2);
                        sizeSpecWidth = MeasureSpec.makeMeasureSpec(diag, MeasureSpec.EXACTLY);
                        sizeSpecHeight = sizeSpecWidth;
                    }
                }

                view.measure(sizeSpecWidth, sizeSpecHeight);
            }
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    boolean state = false;

    //내위치 찾기 함수
    private void startMyLocation() {
        state = true;
        if (mMyLocationOverlay != null) {
            if (!mOverlayManager.hasOverlay(mMyLocationOverlay)) {
                mOverlayManager.addOverlay(mMyLocationOverlay);
            }

            if (mMapLocationManager.isMyLocationEnabled()) {

                if (!mMapView.isAutoRotateEnabled()) {
                    mMyLocationOverlay.setCompassHeadingVisible(true);

                    mMapCompassManager.enableCompass();

                    mMapView.setAutoRotateEnabled(true, false);

                    mMapContainerView.requestLayout();
                } else {
                    stopMyLocation();
                }

                mMapView.postInvalidate();
            } else {
                boolean isMyLocationEnabled = mMapLocationManager.enableMyLocation(true);
                if (!isMyLocationEnabled) {
                    Toast.makeText(MyloactionActivty.this, "Please enable a My Location source in system settings",
                            Toast.LENGTH_LONG).show();

                    Intent goToSettings = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(goToSettings);

                    return;
                }
            }
        }
    }

    //현재 내 위치 서버로 저장
    private class GpsAsyncTask extends AsyncTask<Void, Void, Void> {
        String parser_txt;

        @Override
        protected void onPreExecute() {
        }
        @Override
        protected Void doInBackground(Void... params) {
            // 내 위도 경도 전달
            postData(Double.toString(lat), Double.toString(lon));
            return null;
        }

        protected void onPostExecute(Void content) {
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        public void postData(String lat, String lng) {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://cnscap.dothome.co.kr/insert_gps.php");
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                //내 폰번호 가져옴
                TelephonyManager mgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                //폰번 가져오면 +82로 가져오는걸  0으로 바꿔서 010XXXXXXX 이런 형태로 만듬
                nameValuePairs.add(new BasicNameValuePair("person_id", mgr.getLine1Number().replace("+82", "0")));
                //위도
                nameValuePairs.add(new BasicNameValuePair("lat", lat));
                //경도
                nameValuePairs.add(new BasicNameValuePair("lng", lng));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                parser_txt = EntityUtils.toString(response.getEntity());
                Log.d("response", parser_txt);


            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }
        }
    }

    public class GetgpsAsyncTask extends AsyncTask<Void, Void, Void> {
        String parser_txt;
        boolean result_state = false;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {

            postData();
            return null;
        }

        protected void onPostExecute(Void content) {
            NMapOverlayManager mOverlayManager = new NMapOverlayManager(MyloactionActivty.this, mMapView, mMapViewerResourceProvider);

            int markerId = NMapPOIflagType.PIN;
            String gps_set[] = parser_txt.split(",");
            //연결한 기기 위치 POI핀으로 표시
            NMapPOIdata poiData = new NMapPOIdata(1, mMapViewerResourceProvider);
            poiData.beginPOIdata(2);
            poiData.addPOIitem(Double.parseDouble(gps_set[2]), Double.parseDouble(gps_set[1]), "연결한 기기 위치", markerId, 0);
            poiData.endPOIdata();
            // create POI data overlay
            NMapPOIdataOverlay poiDataOverlay = mOverlayManager.createPOIdataOverlay(poiData, null);
        }

        protected void onProgressUpdate(Integer... progress) {

        }
        //기기 위치의 위도 경도를 가지고옴
        public void postData() {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://cnscap.dothome.co.kr/get_gps.php");
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("person_id", set_phonenum));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                parser_txt = EntityUtils.toString(response.getEntity());
                if (parser_txt.equalsIgnoreCase("") || parser_txt == null) {
                    result_state = false;
                } else {
                    result_state = true;
                    Log.d("response", parser_txt);
                }

            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }
        }
    }
}

