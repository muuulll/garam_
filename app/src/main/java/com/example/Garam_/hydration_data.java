package com.example.Garam_;

import android.graphics.drawable.Drawable;

/*
수화 데이터 커스텀 클래스
*/
public class hydration_data {


    private Drawable iconDrawable;
    private String titleStr;

    public void setIcon(Drawable icon) {
        iconDrawable = icon;
    }

    public void setTitle(String title) {
        titleStr = title;
    }

    public Drawable getIcon() {
        return this.iconDrawable;
    }
    public String getTitle() {
        return this.titleStr;
    }

}
