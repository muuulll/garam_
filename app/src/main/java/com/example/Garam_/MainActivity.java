package com.example.Garam_;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
메인 Activity
*/
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ViewPager mPager;
    Button getgps_btn;
    RelativeLayout voice_tts_btn, menu_lin, setting_lin;
    LinearLayout menu_bar, menu_bar2, notice_btn, how_btn, memo_btn, help_btn, hydration_btn, alram_btn, mylocation_btn;
    ImageButton menu_open_btn, menu_close_btn, menu_close_btn2, setting_btn;
    static CheckBox no_alram, sound_alram, vibe_alram;
    EditText phone_num, meter;
    int width, width2;
    private final int MY_PERMISSION_REQUEST_STORAGE = 100;

    //안드로이드 6.0 대응 - 사용자 권한 설정 리스트
    public final String[] MANDATORY_PERMISSIONS = {
            "android.permission.RECORD_AUDIO"
            , "android.permission.ACCESS_FINE_LOCATION"
            , "android.permission.ACCESS_COARSE_LOCATION"
            , "android.permission.READ_SMS"
    };

    @SuppressLint("NewApi")
    private void checkPermission(String[] permissions) {
        //안드로이드 6.0 대응 - 사용자 권한 설정 확인하기
        requestPermissions(permissions, MY_PERMISSION_REQUEST_STORAGE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (android.os.Build.VERSION.SDK_INT >= 23) {
            checkPermission(MANDATORY_PERMISSIONS);
        }

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(Color.argb(255, 0, 150, 136));
        }

        phone_num = (EditText) findViewById(R.id.phone_num);
        meter = (EditText) findViewById(R.id.meter);

        getgps_btn = (Button) findViewById(R.id.getgps_btn);
        getgps_btn.setOnClickListener(this);

        no_alram = (CheckBox) findViewById(R.id.no_alram);
        sound_alram = (CheckBox) findViewById(R.id.sound_alram);
        vibe_alram = (CheckBox) findViewById(R.id.vibe_alram);

        no_alram.setOnClickListener(this);
        sound_alram.setOnClickListener(this);
        vibe_alram.setOnClickListener(this);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new PagerAdapterClass(this));

        mylocation_btn = (LinearLayout) findViewById(R.id.mylocation_btn);
        mylocation_btn.setOnClickListener(this);

        alram_btn = (LinearLayout) findViewById(R.id.alram_btn);
        alram_btn.setOnClickListener(this);

        voice_tts_btn = (RelativeLayout) findViewById(R.id.voice_tts_btn);
        voice_tts_btn.setOnClickListener(this);

        menu_bar = (LinearLayout) findViewById(R.id.menu_bar);

        setting_lin = (RelativeLayout) findViewById(R.id.setting_lin);
        setting_lin.setOnClickListener(this);

        menu_lin = (RelativeLayout) findViewById(R.id.menu_lin);
        menu_lin.setOnClickListener(this);

        menu_open_btn = (ImageButton) findViewById(R.id.menu_open_btn);
        menu_open_btn.setOnClickListener(this);

        menu_close_btn = (ImageButton) findViewById(R.id.menu_close_btn);
        menu_close_btn.setOnClickListener(this);

        notice_btn = (LinearLayout) findViewById(R.id.notice_btn);
        notice_btn.setOnClickListener(this);

        how_btn = (LinearLayout) findViewById(R.id.how_btn);
        how_btn.setOnClickListener(this);

        memo_btn = (LinearLayout) findViewById(R.id.memo_btn);
        memo_btn.setOnClickListener(this);

        help_btn = (LinearLayout) findViewById(R.id.help_btn);
        help_btn.setOnClickListener(this);

        setting_btn = (ImageButton) findViewById(R.id.setting_btn);
        setting_btn.setOnClickListener(this);

        hydration_btn = (LinearLayout) findViewById(R.id.hydration_btn);
        hydration_btn.setOnClickListener(this);

        menu_bar2 = (LinearLayout) findViewById(R.id.menu_bar2);

        menu_close_btn2 = (ImageButton) findViewById(R.id.menu_close_btn2);
        menu_close_btn2.setOnClickListener(this);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        LinearLayout menu_bar = (LinearLayout) this.findViewById(R.id.menu_bar);
        width = menu_bar.getWidth();
        menu_bar.animate().translationX(-width).setDuration(0).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                RelativeLayout menu_lin = (RelativeLayout) findViewById(R.id.menu_lin);
                menu_lin.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });

        LinearLayout menu_bar2 = (LinearLayout) this.findViewById(R.id.menu_bar2);
        width2 = menu_bar2.getWidth();
        menu_bar2.animate().translationX(width2).setDuration(0).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                RelativeLayout menu_lin2 = (RelativeLayout) findViewById(R.id.setting_lin);
                menu_lin2.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.no_alram:
                sound_alram.setChecked(false);
                vibe_alram.setChecked(false);
                break;
            case R.id.sound_alram:
                no_alram.setChecked(false);
                break;
            case R.id.vibe_alram:
                no_alram.setChecked(false);
                break;
            case R.id.getgps_btn:
                if (!phone_num.getText().toString().equalsIgnoreCase("") && !meter.getText().toString().equalsIgnoreCase("")) {
                    GetgpsAsyncTask getgpsAsyncTask = new GetgpsAsyncTask();
                    getgpsAsyncTask.execute();
                } else {
                    Toast.makeText(this, "번호, 거리를 입력해주세요", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.mylocation_btn:
                intent = new Intent(this, MyloactionActivty.class);
                intent.putExtra("phonenum", phone_num.getText().toString());
                startActivity(intent);
                break;
            case R.id.alram_btn:
                intent = new Intent(this, AlramActivity.class);
                intent.putExtra("phonenum", phone_num.getText().toString());
                intent.putExtra("meterset", meter.getText().toString());
                startActivity(intent);
                break;
            case R.id.setting_lin:
                menu_bar_closd2();
                break;
            case R.id.menu_close_btn2:
                menu_bar_closd2();
                break;
            case R.id.hydration_btn:
                intent = new Intent(this, HydrationActivity.class);
                startActivity(intent);
                break;
            case R.id.setting_btn:
                setting_lin.setVisibility(View.VISIBLE);
                menu_bar2.animate().translationX(0).setDuration(500).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {
                    }
                });
                break;
            case R.id.help_btn:
                intent = new Intent(this, HelpActivity.class);
                startActivity(intent);
                break;
            case R.id.memo_btn:
                intent = getPackageManager().getLaunchIntentForPackage("com.android.notepad.action.EDIT_NOTE");
                startActivity(intent);
                break;
            case R.id.how_btn:
                intent = new Intent(this, HowActivity.class);
                startActivity(intent);
                break;
            case R.id.notice_btn:
                intent = new Intent(this, NoticeActivity.class);
                startActivity(intent);
                break;
            case R.id.menu_close_btn:
                menu_bar_closd();
                break;
            case R.id.voice_tts_btn:
                intent = new Intent(this, VoicescannActivity.class);
                startActivity(intent);
                break;
            case R.id.menu_lin:
                menu_bar_closd();
                break;
            case R.id.menu_open_btn:
                menu_lin.setVisibility(View.VISIBLE);
                menu_bar.animate().translationX(0).setDuration(500).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {
                    }
                });
                break;

        }
    }

    void menu_bar_closd() {
        menu_bar.animate().translationX(-menu_bar.getWidth()).setDuration(500).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                menu_lin.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
    }

    void menu_bar_closd2() {
        menu_bar2.animate().translationX(menu_bar2.getWidth()).setDuration(500).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                setting_lin.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
    }


    /**
     * PagerAdapter
     */
    private class PagerAdapterClass extends PagerAdapter {

        private LayoutInflater mInflater;

        public PagerAdapterClass(Context c) {
            super();
            mInflater = LayoutInflater.from(c);
        }

        @Override
        public int getCount() {
            return 27;
        }

        @Override
        public Object instantiateItem(View pager, int position) {
            View v = null;

            int img_arr[] = {R.drawable.a, R.drawable.b, R.drawable.c, R.drawable.d, R.drawable.e, R.drawable.f, R.drawable.g, R.drawable.h, R.drawable.i, R.drawable.j,
                    R.drawable.k, R.drawable.l, R.drawable.m, R.drawable.n, R.drawable.o, R.drawable.p, R.drawable.q, R.drawable.r, R.drawable.s, R.drawable.t,
                    R.drawable.u, R.drawable.v, R.drawable.w, R.drawable.x, R.drawable.y, R.drawable.z, R.drawable.z1};
            String txt_arr[] = {"가격", "가깝다", "가다듬다1", "가다듬다2", "갈림길", "감기1", "감기2", "걱정", "건강", "그것",
                    "길", "돕다", "만나다", "사다", "사랑", "시청1", "시청2", "안녕1", "안녕2", "안녕3",
                    "안녕하십니까1", "안녕하십니까2", "안녕하십니까3", "이것", "저것", "뜻 쓰시오", "뜻 쓰시오"};

            v = mInflater.inflate(R.layout.main_scroll_img_page, null);
            ImageView img_v = (ImageView) v.findViewById(R.id.main_img);
            img_v.setImageResource(img_arr[position]);
            TextView txt_v = (TextView) v.findViewById(R.id.main_txt);
            txt_v.setText(txt_arr[position]);


            ((ViewPager) pager).addView(v, 0);

            return v;
        }

        @Override
        public void destroyItem(View pager, int position, Object view) {
            ((ViewPager) pager).removeView((View) view);
        }

        @Override
        public boolean isViewFromObject(View pager, Object obj) {
            return pager == obj;
        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(View arg0) {
        }

        @Override
        public void finishUpdate(View arg0) {
        }
    }


    //연결한 기기 위치를 서버로 전송하는 클래스
    public class GetgpsAsyncTask extends AsyncTask<Void, Void, Void> {
        //전송 결과값
        String parser_txt;
        //결과 되었는지 상태값
        boolean result_state = false;
        @Override
        protected void onPreExecute() {
        }
        @Override
        protected Void doInBackground(Void... params) {
            //데이터 전달 시작
            postData();
            return null;
        }
        //완료되었을때
        protected void onPostExecute(Void content) {
            if (result_state == false) {
                //검색한 전화번호 정보가 없음
                Toast.makeText(MainActivity.this, "검색한 정보가 없습니다.", Toast.LENGTH_SHORT).show();
                phone_num.setText("");
            }
        }

        protected void onProgressUpdate(Integer... progress) {

        }
        //전화번호를 전달해 해당 기기의 위도 경도를 가져옴
        public void postData() {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://cnscap.dothome.co.kr/get_gps.php");
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                //폰번호 파라미터 전달
                nameValuePairs.add(new BasicNameValuePair("person_id", phone_num.getText().toString()));
                //utf-8 타입으로 변환
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                parser_txt = EntityUtils.toString(response.getEntity());
                //결과가 없으면
                if (parser_txt.equalsIgnoreCase("") || parser_txt == null) {
                    result_state = false;
                } else {
                    //결과가 있으면
                    result_state = true;
                    Log.d("response", parser_txt);
                }

            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }
        }
    }
}
